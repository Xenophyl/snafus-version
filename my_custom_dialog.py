import sys

from PyQt4.QtCore import pyqtSignal, QThread
 
from PyQt4.QtGui import QDialog, QPushButton, \
     QApplication, QVBoxLayout, QComboBox

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import create_engine

from worker import Worker
from managed_engine import ManagedEngine
from managed_scoped_session import ManagedScopedSession

class MyCustomDialog(QDialog):
 
    finish = pyqtSignal()
 
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)

        self.managed_engine = None
       
        layout = QVBoxLayout(self)
       
        # Create QComboBox()-objects
        self.combo_person_title = QComboBox(self)
        self.combo_person_salutation = QComboBox(self)
        self.combo_person_gender = QComboBox(self)
        self.combo_person_religion = QComboBox(self)
        self.combo_person_relationship_status = QComboBox(self)
        self.combo_person_nationality = QComboBox(self)
        self.combo_person_eye_color = QComboBox(self)
        self.combo_person_hair_color = QComboBox(self)
        # Create QPushButton()-objects
        self.pushButton_populate_combo = QPushButton("Re-populate", self)
        self.pushButton_login = QPushButton("LogIn", self)
        self.pushButton_stopp = QPushButton("Stopp", self)
        self.pushButton_close = QPushButton("Close", self)
        # Add all widgets to layout
        layout.addWidget(self.combo_person_title)
        layout.addWidget(self.combo_person_salutation)
        layout.addWidget(self.combo_person_gender)
        layout.addWidget(self.combo_person_religion)
        layout.addWidget(self.combo_person_nationality)
        layout.addWidget(self.combo_person_relationship_status)
        layout.addWidget(self.combo_person_eye_color)
        layout.addWidget(self.combo_person_hair_color)
        layout.addWidget(self.pushButton_login)
        layout.addWidget(self.pushButton_populate_combo)
        layout.addWidget(self.pushButton_stopp)
        layout.addWidget(self.pushButton_close)
        
        # Set all clicked-signale on QPushButton()-objects
        self.pushButton_login.clicked.connect(self.on_login)
        self.pushButton_stopp.clicked.connect(self.on_finish)
        self.pushButton_populate_combo.clicked.connect(self.on_start_select_all)
        self.pushButton_close.clicked.connect(self.close)
 
    def populate_combobox(self, item, combo_box):
        id, text = item
        combo_box.addItem(text)

    def on_login(self):
        dbms ="mysql"
        dbdriver="pymysql"
        dbuser="root"
        dbuser_pwd="xxxx"
        db_server_host="localhost"
        dbport=3306
        db_name="test"
        echo_verbose=False

        try:

            managed_engine = ManagedEngine(dbms=dbms, dbdriver=dbdriver,
                                               dbuser=dbuser, dbuser_pwd=dbuser_pwd,
                                               db_server_host=db_server_host,
                                               dbport=dbport, db_name=db_name,
                                               echo_verbose=echo_verbose)

            self.managed_engine = managed_engine.session_factory
            
        except SQLAlchemyError as Err:
            print "Err", Err

    def on_start_select_all(self):
        
        list_tuple = [ ("person_salutation", self.combo_person_salutation), 
                    ("person_title", self.combo_person_title), 
                    ("person_gender", self.combo_person_gender), 
                    ("person_religion", self.combo_person_religion),
                    ("person_eye_color", self.combo_person_eye_color),
                    ("person_hair_color", self.combo_person_hair_color),
                    ("person_relationship_status", self.combo_person_relationship_status),
                    ("person_nationality", self.combo_person_nationality)]
        
        with ManagedScopedSession(engine=self.managed_engine) as session_that_scoped:

            for category, combobox in list_tuple:
                combobox.clear()
                self.on_start_thread_task(category=category,
                                          combo_box=combobox,
                                          session_scope=session_that_scoped)
       
    def on_start_thread_task(self, category=None, combo_box=None, session_scope=None):        
         task_thread = QThread(self)
         
         task_thread.work = Worker(category=category, combo_box=combo_box, session_scope=session_scope)
         task_thread.work.moveToThread(task_thread)
         
         task_thread.work.notify_item.connect(self.populate_combobox)
         
         task_thread.work.finish_progress.connect(task_thread.quit)
 
         self.finish.connect(task_thread.work.stop)
         
         task_thread.started.connect(task_thread.work.init_object)
         task_thread.finished.connect(task_thread.deleteLater)
         task_thread.start()
 
    def on_finish(self):
         self.finish.emit()
     
def main():
    app = QApplication(sys.argv)
    window = MyCustomDialog()
    window.resize(600, 400)
    window.show()
    sys.exit(app.exec_())
 
if __name__ == "__main__":
    main()
