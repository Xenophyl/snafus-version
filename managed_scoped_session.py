# -*- coding: cp1252 -*-

from sqlalchemy.orm import scoped_session

class ManagedScopedSession(object):
    def __init__(self, engine=None):

        self._engine = engine
        
        self.session = None

        self._Session = scoped_session(self._engine)
 
    def __enter__(self):        
        self.session = self._Session # this is now a scoped session
        return self.session
 
    def __exit__(self, exception, exc_value, traceback):
        try:
            if exception:
                self.session.rollback()
            else:
                self.session.commit()
        finally:
            self.session.close()

